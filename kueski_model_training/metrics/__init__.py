import logging
from typing import Dict

from pyspark.ml import PipelineModel
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.sql import DataFrame


def calculate_metrics(validation_data_frame: DataFrame, model: PipelineModel) -> Dict[str, float]:
    prediction_data_frame = model\
        .transform(validation_data_frame)

    area_under_roc = calculate_metric(prediction_data_frame, "areaUnderROC")
    area_under_pr = calculate_metric(prediction_data_frame, "areaUnderPR")

    result = dict(area_under_roc=area_under_roc, area_under_pr=area_under_pr)

    return result


def calculate_metric(prediction_data_frame: DataFrame, metric_name: str) -> float:
    evaluator = BinaryClassificationEvaluator(labelCol="label", rawPredictionCol="prediction", metricName=metric_name)
    metric_value = evaluator.evaluate(prediction_data_frame)
    logging.info(f"Metric {metric_name}={metric_value}")
    return metric_value


def aprove_metrics(metrics: Dict[str, float], min_area_under_roc: float, min_area_under_pr: float):

    if metrics["area_under_roc"] < min_area_under_roc:
        raise ValueError(f"area_under_roc value {metrics['area_under_roc']} "
                         f"is smaller then minimum allowed {min_area_under_roc}")

    if metrics["area_under_pr"] < min_area_under_pr:
        raise ValueError(f"area_under_pr value {metrics['area_under_pr']} "
                         f"is smaller then minimum allowed {min_area_under_pr}")
