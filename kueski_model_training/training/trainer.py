import logging

from pyspark.ml import PipelineModel, Pipeline
from pyspark.ml.classification import RandomForestClassifier
from pyspark.sql import DataFrame


def train_model(training_data: DataFrame) -> PipelineModel:
    logging.info(f"Training classifier model")
    classifier_model = RandomForestClassifier()
    pipeline = _make_pipeline_model(classifier_model)
    pipeline_model = pipeline.fit(training_data)

    return pipeline_model


def _make_pipeline_model(classifier: RandomForestClassifier) -> Pipeline:
    pipeline = Pipeline(stages=[classifier])

    return pipeline
