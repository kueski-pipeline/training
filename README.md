# Kueski training

Train a Random tree classifier for the credit risk dataset.  

Use `area under ROC` and `area under PR` as evaluation metrics (that was an arbitrary choice, just for this example). If the evaluation metrics are smaller than the specif parameters, the job will crash, breaking the pipeline and ignoring deployment, else, the model artifact is published as an [onnx](https://onnx.ai/) object to the object storage referenced by `s3-endpoint` at `model-output-file` path.

```
Usage: run-training [OPTIONS]

Options:
  -s, --source-glob TEXT          Glob pattern to source data
  -v, --validation-source-glob TEXT
                                  Glob pattern to validation data
  -o, --model-output-file TEXT    Path to where trained model will be saved
  --spark-master TEXT             Spark master string
  --s3-endpoint TEXT              Endpoint to S3 objet storage server,
                                  defaults to S3_ENDPOINT environment variable
  --aws-access-key-id TEXT        Access key to S3 object storage server,
                                  defaults to AWS_ACCESS_KEY_ID environment
                                  variable
  --aws-secret-access-key TEXT    Access key to S3 object storage server,
                                  defaults to AWS_SECRET_ACCESS_KEY
                                  environment variable
  --min-area-under-roc FLOAT      Smallest value allowed for area under ROC,
                                  to consider model aceptable
  --min-area-under-pr FLOAT       Smallest value allowed for area under PR, to
                                  consider model aceptable
  --help                          Show this message and exit.
```