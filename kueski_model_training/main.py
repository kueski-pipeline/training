import logging
import os

import click

from kueski_model_training.metrics import calculate_metrics, aprove_metrics
from kueski_model_training.persistence.data import load_source_data
from kueski_model_training.persistence.model import save_model, create_storage_params
from kueski_model_training.spark_helpers import get_from_environment, create_spark_session
from kueski_model_training.training.trainer import train_model


@click.command()
@click.option("--source-glob", "-s", type=click.STRING, help="Glob pattern to source data")
@click.option("--validation-source-glob", "-v", type=click.STRING, help="Glob pattern to validation data")
@click.option("--model-output-file", "-o", type=click.STRING, help="Path to where trained model will be saved")
@click.option("--spark-master", type=click.STRING, help="Spark master string", default="local[*]")
@click.option("--s3-endpoint", type=click.STRING,
              help="Endpoint to S3 objet storage server, defaults to S3_ENDPOINT environment variable",
              default=get_from_environment("S3_ENDPOINT"))
@click.option("--aws-access-key-id", type=click.STRING,
              help="Access key to S3 object storage server, defaults to AWS_ACCESS_KEY_ID environment variable",
              default=get_from_environment("AWS_ACCESS_KEY_ID"))
@click.option("--aws-secret-access-key", type=click.STRING,
              help="Access key to S3 object storage server, defaults to AWS_SECRET_ACCESS_KEY environment variable",
              default=get_from_environment("AWS_SECRET_ACCESS_KEY"))
@click.option("--min-area-under-roc", type=click.FLOAT, default="0.0",
              help="Smallest value allowed for area under ROC, to consider model aceptable")
@click.option("--min-area-under-pr", type=click.FLOAT, default="0.0",
              help="Smallest value allowed for area under PR, to consider model aceptable")
def main(source_glob: str, validation_source_glob: str, model_output_file: str, spark_master: str,
         s3_endpoint: str, aws_access_key_id: str, aws_secret_access_key: str, min_area_under_roc: float,
         min_area_under_pr: float):

    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    spark_session = create_spark_session(spark_master, s3_endpoint, aws_access_key_id, aws_secret_access_key)
    source_data_frame = load_source_data(spark_session, source_glob)

    trained_model = train_model(source_data_frame)

    validation_data_frame = load_source_data(spark_session, validation_source_glob)
    metrics = calculate_metrics(validation_data_frame, trained_model)
    aprove_metrics(metrics, min_area_under_roc, min_area_under_pr)

    storage_params = create_storage_params(s3_endpoint, aws_access_key_id, aws_secret_access_key)
    save_model(trained_model, model_output_file, spark_session, storage_params)

    logging.info(f"Process finished")


if __name__ == '__main__':
    main()
