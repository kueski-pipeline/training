from typing import Optional, Dict
import os

import boto3
from onnxconverter_common import FloatTensorType
from onnxmltools import convert_sparkml
from pyspark.ml import PipelineModel
from pyspark.sql import SparkSession
from smart_open import open


def save_model(model: PipelineModel, output_path: str, spark_sesion: SparkSession, storage_params: Dict):

    input_schema = [
        ("features", FloatTensorType(shape=(None, 5))),
    ]

    onnx_model = convert_sparkml(model, 'kueski model', input_schema, spark_session=spark_sesion)

    with open(output_path, mode="wb", transport_params=storage_params) as output_file:
        output_file.write(onnx_model.SerializeToString())


def create_storage_params(s3_endpoint: Optional[str], aws_access_key_id: Optional[str],
                          aws_secret_access_key: Optional[str]) -> Dict:
    if s3_endpoint:
        session = boto3.Session()
        client = session.client('s3', endpoint_url=s3_endpoint, use_ssl=False,
                                aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
        transport_params = dict(client=client)
    else:
        transport_params = None

    return transport_params
